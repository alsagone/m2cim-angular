import { Component } from '@angular/core';

@Component({
  selector: 'app-etudiant',
  templateUrl: './etudiant.component.html',
  styleUrls: ['./etudiant.component.scss']
})
export class EtudiantComponent {
  nom = "Toto"
  prenom = "Martin"
  statusEtu = "Absent" 
  getNomEtudiant (){
    return this.nom; 
  }
}
